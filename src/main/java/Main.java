import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

public class Main {
    public static void main(String[] args) {
        Runtime runtime = Runtime.instance();
        Profile profile = new ProfileImpl();

        profile.setParameter(Profile.MAIN_HOST, "localhost");
        profile.setParameter(Profile.GUI, "true");

        ContainerController containerController = runtime.createMainContainer(profile);

        try {
            AgentController newAgent1 = containerController
                    .createNewAgent("First_Agent", "FirstAgent", null);
            AgentController newAgent2 = containerController
                    .createNewAgent("Second_Agent", "SecondAgent", null);

            newAgent1.start();
            newAgent2.start();
        } catch (StaleProxyException e){
            e.printStackTrace();
        }
    }
}
