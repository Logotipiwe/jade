import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

import javax.swing.*;

public class SecondAgent extends Agent {
    @Override
    protected void setup() {
        System.out.println("Second agent");
        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage message = receive();
                if(message != null){
                    JOptionPane.showMessageDialog(null,
                    "Recieved message from"
                            + message.getSender().getLocalName()
                            + " :" + message.getContent());
                } else {
                    block();
                }
            }
        });
    }
}
